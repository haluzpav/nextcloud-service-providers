import os

from convert_currencies import convert_currencies
from sort_table import sort_table

filename_src = r'README_src.md'
filename_partial = r'README.md.part'
filename_dst = r'README.md'

target_currency = 'EUR'

if os.path.exists(filename_partial):
    os.remove(filename_partial)

convert_currencies(filename_src, filename_partial, target_currency)
sort_table(filename_partial, filename_partial)

if os.path.exists(filename_dst):
    os.remove(filename_dst)
os.rename(filename_partial, filename_dst)
