from dataclasses import dataclass

from sortedcontainers import SortedList


@dataclass
class TableLine:
    line: str
    price: float
    storage: int


def sort_table(filename_src, filename_dst):
    with open(filename_src) as file_src:
        lines = file_src.readlines()
    with open(filename_dst, 'w') as file_dst:
        passed_header = False
        loading_table = False
        table = SortedList(key=lambda x: (x.price / x.storage, -x.storage, x.line))
        for line in lines:
            if not line.startswith('| '):
                if loading_table:
                    for table_line in table:
                        file_dst.write(table_line.line)
                    passed_header = False
                    loading_table = False
                    table.clear()
                file_dst.write(line)
                continue
            if not passed_header:
                passed_header = True
                file_dst.write(line)
                continue
            loading_table = True
            split = [value.strip() for value in line.split('|')]
            price, storage = float(split[5]), int(split[6])
            table.add(TableLine(line, price, storage))


if __name__ == '__main__':
    sort_table('README.md', 'README_sorted_TEMP.md')
