import re

from forex_python.converter import CurrencyRates

float_regex = re.compile(r'([0-9]+(\.[0-9]+)?)')


def replace_match_with(src_string, match: re.Match, replacement):
    return src_string[:match.start()] + replacement + src_string[match.end():]


def _convert_match(match: re.Match, rate):
    amount_orig = float(match.group(1))
    amount_converted = amount_orig / rate
    return f'{amount_converted:.2f}'


def convert_currencies(filename_src, filename_dst, target_currency):
    rates = CurrencyRates().get_rates(target_currency)

    with open(filename_src) as file:
        with open(filename_dst, 'w') as file_partial:
            for line in file:
                while match := re.search(f'{float_regex.pattern} {target_currency}', line):
                    line = replace_match_with(line, match, _convert_match(match, 1))
                for currency, rate in rates.items():
                    while match := re.search(f'{float_regex.pattern} {currency}', line):
                        line = replace_match_with(line, match, _convert_match(match, rate))
                file_partial.write(line)


if __name__ == '__main__':
    convert_currencies('README_src.md', 'README.md', 'EUR')
