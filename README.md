# Nextcloud Service Providers

List of companies providing [Nextcloud](https://nextcloud.com/) as a service for personal use and reasonable prices.

| Name                                                                                                                                             | HQ | Servers          | Crypto-payment | Min. EUR / month | Min. GB | Office suite \[EUR\]       | Admin access \[EUR\]  |
|:-------------------------------------------------------------------------------------------------------------------------------------------------|:---|:-----------------|:---------------|-----------------:|--------:|:---------------------------|:----------------------|
| [Spry Servers](https://www.spryservers.net/sprycloud)                                                                                            | us | us               | yes            |             0.00 |      10 | Collabora (free)           | no                    |
| [OwnCube](https://owncube.com) ([free](https://owncube.com/free_next_en.php), [single](https://billing.owncube.com/cart.php?gid=38))<sup>1</sup> | at | de,more          | yes            |             0.00 |       5 | both (free?)               | from 1.50 / month     |
| [Webo Hosting](https://webo.hosting/en/webo-cloud-en)                                                                                            | si | de,fi            | no             |             0.00 |       5 | both (free)                | from 6.00 / month     |
| [Disroot](https://disroot.org/en/services/nextcloud)                                                                                             | nl |                  | yes            |             0.00 |       2 | no                         | no                    |
| [thegood.cloud](https://thegood.cloud)<sup>2</sup>                                                                                               | nl | nl               |                |             0.00 |       2 | Collabora (3.00 / month)   | no                    |
| [OwnDrive](https://owndrive.com)                                                                                                                 | no | eu               |                |             0.00 |       1 | no                         | from 7.00 / month     |
| [oCloud.de](https://next.ocloud.de/products.php)<sup>1</sup>                                                                                     | de | de               | no             |             0.00 |       1 | both (free)                | yes                   |
| [woelkli Cloud Storage](https://woelkli.com/)                                                                                                    | ch | ch               | no             |             0.00 |       1 | both (220.63 / year each)  | from 367.71 / year    |
| [CyanSpace](https://cyanspace.net)                                                                                                               | gb |                  |                |             3.51 |     100 | no                         | no                    |
| [Hostiso](https://hostiso.com/nextcloud-hosting)<sup>2</sup>                                                                                     | ch | all              | yes            |             3.60 |     100 | no                         | from 18.05 / month    |
| [Your Own Net](https://yourownnet.net/nextcloud)<sup>2</sup>                                                                                     | fr | nl,fr,de,ca,more |                |             0.83 |      15 | both (free)                | yes (contact support) |
| [cloudlay](https://www.cloudlay.com)<sup>2</sup>                                                                                                 | de | de               | yes            |             9.99 |     150 | no                         | yes                   |
| [VeeroTech](https://www.veerotech.net/nextcloud-hosting)<sup>2</sup>                                                                             | us |                  | no             |             1.88 |      25 | no                         | yes                   |
| [Service Metric](https://www.servicemetric.com/portal/link.php?id=1)<sup>2</sup>                                                                 | us | us               | no             |             0.38 |       5 | no                         | from 12.64 / month    |
| [Portknox.net](https://portknox.net/en)<sup>1, 2</sup>                                                                                           | de | de               | no             |             2.42 |      15 | Collabora (80.00 / year)   | from 69.00 / year     |
| [Kinamo SA](https://www.kinamo.be/en/cloud/storage)<sup>2</sup>                                                                                  | be | be               | no             |             2.50 |      15 | no                         | from 10.00 / month    |
| [CiviHosting](https://civihosting.com/nextcloud-hosting)                                                                                         | us |                  | no             |            18.05 |     100 | Collabora (non-zero price) | from 72.20 / month    |
| [Netways](https://nws.netways.de/contracts/new?product_id=20-nextcloud)                                                                          | de |                  |                |             9.99 |      50 | Collabora (free)           | yes                   |
| [deskonline.cloud](https://deskonline.cloud)                                                                                                     | au | au               |                |            13.54 |      50 | OnlyOffice (free)          | from 1173.29 / month  |
| [MyDreams](https://www.mydreams.cz/en/saas-servers/nextcloud-hosting-en.html)<sup>1</sup>                                                        | cz |                  | yes            |             2.94 |      10 | no                         | no                    |
| [GreenAnt Networks](https://www.greenant.net/cloud-storage)                                                                                      | au | au               | yes            |             3.12 |      10 | Collabora (free)           | from 40.58 / month    |
| [K and T Host](https://www.knthost.com/nextcloud-hosting)                                                                                        | us |                  |                |             7.22 |      20 | no                         | yes                   |
| [yourcloud.asia](https://yourcloud.asia/)                                                                                                        | tw | hk               |                |             2.71 |       5 | no                         | no                    |
| [GreenNet Ltd](https://www.greennet.org.uk/internet-services/cloud-storage)<sup>2</sup>                                                          | gb | gb               | no             |             5.85 |      10 | no                         | from 210.45 / year    |
| [Thexyz Inc](https://www.thexyz.com/nextcloud.php)                                                                                               | us | us,more          | no             |             2.66 |       1 | no                         | from 13.54 / month    |

*Last updated of data between 2019-12-24 and 2019-12-25. Prices converted by ECB rates on 2019-12-25. Sorted by price / GB.*

<sup>1</sup>: Incomplete English translations,
<sup>2</sup>: Annual payment required

## Sources

The majority of providers were scraped from [Nextcloud's list of providers](https://github.com/nextcloud/nextcloud.com/blob/master/assets/providers.json).

Many providers aren't included for some of the following reasons:

- Unspecified pricing model / only per request
- Website not in English
- Ridiculous prices
- Not for personal use
