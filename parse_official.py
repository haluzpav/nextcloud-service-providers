import datetime
import json
import time
import webbrowser
from dataclasses import dataclass
from typing import List

import jsons

"""
Source: https://github.com/nextcloud/nextcloud.com/blob/master/assets/providers.json
"""

src_filename = 'providers.json'
dst_header = 'header.csv'


@dataclass
class Provider:
    title: str
    url: str
    flags: List[str]
    freeplans: bool
    supports: str
    specializes: List[str]
    imagename: str


def now():
    return datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()


def parse():
    with open(src_filename) as file:
        d = json.load(file)
        return jsons.load(d, List[Provider])


def fill_in(providers: List[Provider], dst_filename: str, continue_from: int):
    if continue_from == 0:
        with open(dst_header) as file:
            header = file.readline()
        with open(dst_filename, 'w') as file:
            file.write(header + '\n')
    for i, p in enumerate(providers):
        print(f'{i} / {len(providers)}: {p.title}')
        if i < continue_from:
            print('\tskipping')
            continue
        if p.supports == "organization":
            print('\tskipping organization only')
            continue
        webbrowser.open(p.url)
        flags = '"' + ','.join(p.flags) + '"'
        with open(dst_filename, 'a') as file:
            file.write(f'{p.title},,{flags},,,,,,,,{now()},{p.url}\n')
        input()


def main():
    providers = parse()

    dst_filename = f'providers_{int(time.time())}.csv'
    continue_from = 0
    fill_in(providers, dst_filename, continue_from)


if __name__ == '__main__':
    main()
