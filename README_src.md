# Nextcloud Service Providers

List of companies providing [Nextcloud](https://nextcloud.com/) as a service for personal use and reasonable prices.

| Name                                                                                                                                             | HQ | Servers          | Crypto-payment | Min. EUR / month | Min. GB | Office suite \[EUR\]       | Admin access \[EUR\]  |
|:-------------------------------------------------------------------------------------------------------------------------------------------------|:---|:-----------------|:---------------|-----------------:|--------:|:---------------------------|:----------------------|
| [Hostiso](https://hostiso.com/nextcloud-hosting)<sup>2</sup>                                                                                     | ch | all              | yes            |         3.99 USD |     100 | no                         | from 20 USD / month   |
| [OwnCube](https://owncube.com) ([free](https://owncube.com/free_next_en.php), [single](https://billing.owncube.com/cart.php?gid=38))<sup>1</sup> | at | de,more          | yes            |            0 EUR |       5 | both (free?)               | from 1.5 EUR / month  |
| [Kinamo SA](https://www.kinamo.be/en/cloud/storage)<sup>2</sup>                                                                                  | be | be               | no             |          2.5 EUR |      15 | no                         | from 10 EUR / month   |
| [Service Metric](https://www.servicemetric.com/portal/link.php?id=1)<sup>2</sup>                                                                 | us | us               | no             |         0.42 USD |       5 | no                         | from 14 USD / month   |
| [woelkli Cloud Storage](https://woelkli.com/)                                                                                                    | ch | ch               | no             |            0 CHF |       1 | both (240 CHF / year each) | from 400 CHF / year   |
| [OwnDrive](https://owndrive.com)                                                                                                                 | no | eu               |                |            0 EUR |       1 | no                         | from 7 EUR / month    |
| [K and T Host](https://www.knthost.com/nextcloud-hosting)                                                                                        | us |                  |                |            8 USD |      20 | no                         | yes                   |
| [oCloud.de](https://next.ocloud.de/products.php)<sup>1</sup>                                                                                     | de | de               | no             |            0 EUR |       1 | both (free)                | yes                   |
| [Portknox.net](https://portknox.net/en)<sup>1, 2</sup>                                                                                           | de | de               | no             |         2.42 EUR |      15 | Collabora (80 EUR / year)  | from 69 EUR / year    |
| [Webo Hosting](https://webo.hosting/en/webo-cloud-en)                                                                                            | si | de,fi            | no             |            0 EUR |       5 | both (free)                | from 6 EUR / month    |
| [Your Own Net](https://yourownnet.net/nextcloud)<sup>2</sup>                                                                                     | fr | nl,fr,de,ca,more |                |         0.83 EUR |      15 | both (free)                | yes (contact support) |
| [GreenAnt Networks](https://www.greenant.net/cloud-storage)                                                                                      | au | au               | yes            |            5 AUD |      10 | Collabora (free)           | from 65 AUD / month   |
| [Netways](https://nws.netways.de/contracts/new?product_id=20-nextcloud)                                                                          | de |                  |                |         9.99 EUR |      50 | Collabora (free)           | yes                   |
| [CiviHosting](https://civihosting.com/nextcloud-hosting)                                                                                         | us |                  | no             |           20 USD |     100 | Collabora (non-zero price) | from 80 USD / month   |
| [GreenNet Ltd](https://www.greennet.org.uk/internet-services/cloud-storage)<sup>2</sup>                                                          | gb | gb               | no             |            5 GBP |      10 | no                         | from 180 GBP / year   |
| [cloudlay](https://www.cloudlay.com)<sup>2</sup>                                                                                                 | de | de               | yes            |         9.99 EUR |     150 | no                         | yes                   |
| [yourcloud.asia](https://yourcloud.asia/)                                                                                                        | tw | hk               |                |            3 USD |       5 | no                         | no                    |
| [deskonline.cloud](https://deskonline.cloud)                                                                                                     | au | au               |                |           15 USD |      50 | OnlyOffice (free)          | from 1300 USD / month |
| [Spry Servers](https://www.spryservers.net/sprycloud)                                                                                            | us | us               | yes            |            0 USD |      10 | Collabora (free)           | no                    |
| [CyanSpace](https://cyanspace.net)                                                                                                               | gb |                  |                |            3 GBP |     100 | no                         | no                    |
| [Thexyz Inc](https://www.thexyz.com/nextcloud.php)                                                                                               | us | us,more          | no             |         2.95 USD |       1 | no                         | from 15 USD / month   |
| [thegood.cloud](https://thegood.cloud)<sup>2</sup>                                                                                               | nl | nl               |                |            0 EUR |       2 | Collabora (3 EUR / month)  | no                    |
| [MyDreams](https://www.mydreams.cz/en/saas-servers/nextcloud-hosting-en.html)<sup>1</sup>                                                        | cz |                  | yes            |           75 CZK |      10 | no                         | no                    |
| [VeeroTech](https://www.veerotech.net/nextcloud-hosting)<sup>2</sup>                                                                             | us |                  | no             |         2.08 USD |      25 | no                         | yes                   |
| [Disroot](https://disroot.org/en/services/nextcloud)                                                                                             | nl |                  | yes            |            0 EUR |       2 | no                         | no                    |

*Last updated of data between 2019-12-24 and 2019-12-25. Prices converted by ECB rates on 2019-12-25. Sorted by price / GB.*

<sup>1</sup>: Incomplete English translations,
<sup>2</sup>: Annual payment required

## Sources

The majority of providers were scraped from [Nextcloud's list of providers](https://github.com/nextcloud/nextcloud.com/blob/master/assets/providers.json).

Many providers aren't included for some of the following reasons:

- Unspecified pricing model / only per request
- Website not in English
- Ridiculous prices
- Not for personal use
